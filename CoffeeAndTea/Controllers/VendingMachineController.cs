﻿using CoffeeAndTea.Models;
using CoffeeAndTeaLib;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Mvc;


namespace CoffeeAndTea.Controllers
{
    [ApiController]
    [Microsoft.AspNetCore.Mvc.Route("[controller]")]
    public class VendingMachineController : ControllerBase
    {
        public VendingMachine VendingMachine { get; }

        public VendingMachineController(VendingMachine vendingMachine)
        {
            VendingMachine = vendingMachine;
        }

        [HttpGet]
        [Microsoft.AspNetCore.Mvc.Route("GetDrinks")]
        public JsonResult GetDrinks()
        {
            return new JsonResult(VendingMachine.GetDrinks());
        }

        [HttpPost]
        [Microsoft.AspNetCore.Mvc.Route("MakeDrink")]
        public JsonResult MakeDrink([FromBody] DrinkRequest drinkRequest)
        {
            var result = VendingMachine.MakeDrink(drinkRequest.DrinkName);
            return new JsonResult(result);
        }
    }
}