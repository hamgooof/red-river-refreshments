﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

function VendingMachineModel() {
    const _this = this;
    _this.enabled = ko.observable(false);
    _this.makingDrink = ko.observable(false);
    _this.drinkOptions = ko.observableArray();
    _this.madeDrink = ko.observable();
    _this.drinkMadeSteps = ko.observableArray();


    _this.makeDrink = function (selectedDrink) {
        $.ajax({
            url: "VendingMachine/MakeDrink",
            type: 'POST',
            data: JSON.stringify({drinkName: selectedDrink}),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (successMessages) {
                _this.drinkMadeSteps(successMessages);
                _this.madeDrink(selectedDrink);
            },
            error: function (err) {
                _this.drinkMadeSteps(["Something went wrong!"]);
            },
            complete: function () {
                _this.makingDrink(false);
            }
        });
    }
    _this.hasDrinkMadeSteps = ko.computed(function () {
        return _this.drinkMadeSteps() && _this.drinkMadeSteps().length > 0;
    })
    _this.selectDrink = function (selectedDrink) {
        //TODO Move Clear values to separate function
        _this.madeDrink("");
        _this.drinkMadeSteps([]);

        //Get server to make drink - DELAYED otherwise might be too fast
        _this.makingDrink(true);
        setTimeout(function () {
            _this.makeDrink(selectedDrink)
        }, 1500);


    }


    //Load Data
    $.get("VendingMachine/GetDrinks", function (drinks) {
        if (drinks) {
            _this.drinkOptions(drinks);
        }
    });

    this.enabled(true);

}

ko.applyBindings(new VendingMachineModel());