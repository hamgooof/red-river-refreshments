﻿using System.Text.Json.Serialization;

namespace CoffeeAndTea.Models
{
    public class DrinkRequest
    {
        [JsonPropertyName("drinkName")]
        public string DrinkName { get; set; }
    }
}