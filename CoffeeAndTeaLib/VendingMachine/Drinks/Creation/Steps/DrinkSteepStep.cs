using CoffeeAndTeaLib.Extensions;

namespace CoffeeAndTeaLib
{
    public class DrinkSteepStep : DrinkStep
    {
        private readonly VendingMachineIngredients _source;
        private readonly VendingMachineIngredients _target;


        public DrinkSteepStep(VendingMachineIngredients source, VendingMachineIngredients target) : base(
            VendingMachineAction.Steep)
        {
            _source = source;
            _target = target;
        }

        public override string doStep()
        {
            return $"Steeping the {_source.GetHumanReadableName()} in {_target.GetHumanReadableName()}";
        }
    }
}