namespace CoffeeAndTeaLib
{
    public class DrinkPourToCupStep : DrinkStep
    {
        private readonly string drinkMix;

        public DrinkPourToCupStep(string drinkMix) : base(VendingMachineAction.Pour)
        {
            this.drinkMix = drinkMix;
        }

        public override string doStep()
        {
            return $"Pouring {drinkMix} into cup...";
        }
    }
}