using CoffeeAndTeaLib.Extensions;

namespace CoffeeAndTeaLib
{
    public class DrinkBoilStep : DrinkStep
    {
        private readonly VendingMachineIngredients _ingredient;

        public DrinkBoilStep(VendingMachineIngredients ingredient) : base(VendingMachineAction.Boil)
        {
            _ingredient = ingredient;
        }

        public override string doStep()
        {
            return $"Boiling {_ingredient.GetHumanReadableName()}...";
        }
    }
}