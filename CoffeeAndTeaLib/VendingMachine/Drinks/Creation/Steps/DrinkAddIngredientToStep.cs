namespace CoffeeAndTeaLib
{
    public class DrinkAddIngredientToStep : DrinkStep
    {
        private readonly VendingMachineIngredients _source;
        private readonly VendingMachineIngredients _target;

        public DrinkAddIngredientToStep(VendingMachineIngredients source, VendingMachineIngredients target) : base(
            VendingMachineAction.Add)
        {
            _source = source;
            _target = target;
        }

        public override string doStep()
        {
            return $"Adding {_source} to {_target}";
        }
    }
}