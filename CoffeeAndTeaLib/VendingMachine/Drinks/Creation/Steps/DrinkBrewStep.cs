namespace CoffeeAndTeaLib
{
    public class DrinkBrewStep : DrinkStep
    {
        private readonly VendingMachineIngredients _source;


        public DrinkBrewStep(VendingMachineIngredients source) : base(VendingMachineAction.Steep)
        {
            _source = source;
        }

        public override string doStep()
        {
            return $"Brewing the {_source}";
        }
    }
}