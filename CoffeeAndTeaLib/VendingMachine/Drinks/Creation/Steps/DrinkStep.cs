﻿namespace CoffeeAndTeaLib
{
    public abstract class DrinkStep
    {
        private readonly VendingMachineAction _action;

        public DrinkStep(VendingMachineAction action)
        {
            _action = action;
        }

        public abstract string doStep();
    }
}