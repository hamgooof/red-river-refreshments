﻿namespace CoffeeAndTeaLib
{
    public enum VendingMachineIngredients
    {
        Water,
        Tea,
        Lemon,
        Sugar,
        Milk,
        ChocolatePowder,
        Cup,
        Coffee
    }
}