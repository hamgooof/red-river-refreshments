﻿namespace CoffeeAndTeaLib
{
    public enum VendingMachineAction
    {
        Boil,
        Steep,
        Brew,
        Pour,
        Add
    }
}