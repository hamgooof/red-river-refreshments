﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using CoffeeAndTeaLib.Drinks;
using CoffeeAndTeaLib.Exceptions;

namespace CoffeeAndTeaLib
{
    public class VendingMachine
    {
        public readonly List<VendingDrink> availableDrinks = new List<VendingDrink>();

        public VendingMachine()
        {
            LoadAvailableDrinks();
        }

        private void LoadAvailableDrinks()
        {
            availableDrinks.Add(new LemonTeaVendingDrink());
            availableDrinks.Add(new CoffeeVendingDrink());
            availableDrinks.Add(new HotChocolateDrink());
        }

        public List<string> GetDrinks()
        {
            return availableDrinks.Select(p => p.DrinkName).ToList();
        }

        public bool HasDrink(string drinkName)
        {
            return availableDrinks.Any(p => p.DrinkName.Equals(drinkName, StringComparison.InvariantCultureIgnoreCase));
        }


        public List<string> MakeDrink(string drinkName)
        {
            // Find the drink we want to make
            var drink = availableDrinks.FirstOrDefault(p =>
                p.DrinkName.Equals(drinkName, StringComparison.InvariantCultureIgnoreCase));
            if (drink == null)
                throw new DrinkNotFoundException(drinkName);

            // Do the steps & capture results
            var steps = drink.GetDrinkSteps();
            var stepResults = new List<string>();

            //Using 'foreach' loop instead of LINQ as if we want to add error handling (out of water!) 
            foreach (var drinkStep in steps)
            {
                var drinkStepResult = drinkStep.doStep();
                stepResults.Add(drinkStepResult);
            }

            return stepResults;
        }
    }
}