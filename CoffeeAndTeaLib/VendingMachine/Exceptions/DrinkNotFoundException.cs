﻿using System;

namespace CoffeeAndTeaLib.Exceptions
{
    public class DrinkNotFoundException : Exception
    {
        private readonly string _drinkName;

        public DrinkNotFoundException(string drinkName) : base($"Could not find drink {drinkName}")
        {
            _drinkName = drinkName;
        }
    }
}