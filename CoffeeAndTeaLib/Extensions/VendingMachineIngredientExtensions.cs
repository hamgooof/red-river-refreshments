﻿using System;

namespace CoffeeAndTeaLib.Extensions
{
    public static class VendingMachineIngredientExtensions
    {
        public static string GetHumanReadableName(this VendingMachineIngredients ingred)
        {
            switch (ingred)
            {
                case VendingMachineIngredients.Water:
                case VendingMachineIngredients.Tea:
                case VendingMachineIngredients.Lemon:
                case VendingMachineIngredients.Sugar:
                case VendingMachineIngredients.Milk:
                case VendingMachineIngredients.Cup:
                case VendingMachineIngredients.Coffee:
                    return ingred.ToString();
                case VendingMachineIngredients.ChocolatePowder:
                    return "chocolate powder";
              
                default:
                    throw new ArgumentOutOfRangeException(nameof(ingred), ingred, null);
            }
        }
    }
}