﻿using System.Collections.Generic;

namespace CoffeeAndTeaLib.Drinks
{
    public class CoffeeVendingDrink : VendingDrink
    {
        public CoffeeVendingDrink() : base("Coffee")
        {
        }

        public override List<DrinkStep> GetDrinkSteps()
        {
            var steps = new List<DrinkStep>()
            {
                new DrinkBoilStep(VendingMachineIngredients.Water),
                new DrinkBrewStep(VendingMachineIngredients.Coffee),
                new DrinkPourToCupStep(DrinkName),
                new DrinkAddIngredientToStep(VendingMachineIngredients.Sugar, VendingMachineIngredients.Cup),
                new DrinkAddIngredientToStep(VendingMachineIngredients.Milk, VendingMachineIngredients.Cup),
            };

            return steps;
        }
    }
}