﻿using System.Collections.Generic;

namespace CoffeeAndTeaLib.Drinks
{
    public abstract class VendingDrink
    {
        public string DrinkName { get; }

        protected VendingDrink(string drinkName)
        {
            DrinkName = drinkName;
        }

        public abstract List<DrinkStep> GetDrinkSteps();
    }
}