﻿using System.Collections.Generic;

namespace CoffeeAndTeaLib.Drinks
{
    public class LemonTeaVendingDrink : VendingDrink
    {
        public LemonTeaVendingDrink() : base("Lemon Tea")
        {
        }


        public override List<DrinkStep> GetDrinkSteps()
        {
            var steps = new List<DrinkStep>()
            {
                new DrinkBoilStep(VendingMachineIngredients.Water),
                new DrinkSteepStep(VendingMachineIngredients.Water, VendingMachineIngredients.Tea),
                new DrinkPourToCupStep(DrinkName),
                new DrinkAddIngredientToStep(VendingMachineIngredients.Lemon, VendingMachineIngredients.Cup)
            };

            return steps;
        }
    }
}