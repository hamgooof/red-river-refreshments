﻿using System.Collections.Generic;

namespace CoffeeAndTeaLib.Drinks
{
    public class HotChocolateDrink: VendingDrink
    {
        public HotChocolateDrink() : base("Hot Chocolate")
        {
        }


        public override List<DrinkStep> GetDrinkSteps()
        {
            var steps = new List<DrinkStep>()
            {
                new DrinkBoilStep(VendingMachineIngredients.Water),
                new DrinkAddIngredientToStep(VendingMachineIngredients.ChocolatePowder, VendingMachineIngredients.Cup),
                new DrinkPourToCupStep(DrinkName),
            };

            return steps;
        }
    }
}