using CoffeeAndTeaLib;
using NUnit.Framework;

namespace CoffeeAndTeamLibTests
{
    public class VendingMachineTests
    {
        [Test]
        public void VendingMachine_Ctor_Test()
        {
            var vendingMachine = new VendingMachine();
            Assert.IsNotNull(vendingMachine);
        }

        [Test]
        public void VendingMachine_GetDrinks_Test()
        {
            const int expectedDrinkLength = 2;
            string[] expectedDrinkNames = { "Coffee", "Lemon Tea" };

            var vendingMachine = new VendingMachine();
            var drinks = vendingMachine.GetDrinks();

            //Check length
            Assert.AreEqual(expectedDrinkLength, drinks.Count);

            foreach (var drink in expectedDrinkNames)
            {
                Assert.IsTrue(drinks.Contains(drink), "Could not find " + drink + " in vending machine!");
            }
        }

        [Test]
        public void VendingMachine_HasDrink_True_Test()
        {
            const string drinkName = "COFFEE";

            var vendingMachine = new VendingMachine();
            var actual = vendingMachine.HasDrink(drinkName);

            Assert.IsTrue(actual);
        }

        [Test]
        public void VendingMachine_HasDrink_False_Test()
        {
            const string drinkName = "RED BULL";

            var vendingMachine = new VendingMachine();
            var actual = vendingMachine.HasDrink(drinkName);

            Assert.IsFalse(actual);
        }

        [Test]
        public void VendingMachine_HasDrink_Null_False_Test()
        {
            const string drinkName = null;

            var vendingMachine = new VendingMachine();
            var actual = vendingMachine.HasDrink(drinkName);

            Assert.IsFalse(actual);
        }

        [Test]
        public void VendingMachine_Coffee_GetSteps_Test()
        {
            const string drinkName = "Coffee";
            const int expectedSteps = 5;

            var vendingMachine = new VendingMachine();
            var actual = vendingMachine.MakeDrink(drinkName);

            Assert.AreEqual(expectedSteps, actual.Count);
        }
    }
}